package com.safebear.auto.tests;

import com.safebear.auto.pages.LoginPage;
import com.safebear.auto.pages.ToolsPage;
import com.safebear.auto.services.UserProvider;
import com.safebear.auto.utils.Properties;
import cucumber.api.PendingException;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

public class StepDefs {

    WebDriver driver;
    LoginPage loginPage;
    ToolsPage toolsPage;
    UserProvider userProvider = new UserProvider();

    @Before
    public void setUp() {
        driver = Properties.getDriver();
        toolsPage = new ToolsPage(driver);
        loginPage = new LoginPage(driver);
    }

    @After
    public void tearDown() {
        try {
            Thread.sleep(Integer.parseInt(System.getProperty("sleep", "1000")));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        driver.quit();
    }

    @Given("^I nav to the login page$")
    public void i_nav_to_the_login_page() throws Throwable {
        //go to the login page
        driver.get(Properties.getUrl());
        //Check we're on the login page
        Assert.assertEquals(loginPage.getPageTitle(), loginPage.getExpectedPageTitle(), "We're on the wrong page, or the title is incorrect");
    }

    @When("^I enter login details for (.+)$")
    public void i_enter_login_details_for_a_user(String userType) throws Throwable {
        if (null == userProvider.getUser(userType)) {
            Properties.captureScreenShot(driver, Properties.generateScreenshotFileName("enterLoginDetails"));
            Assert.fail("The user type supplied is not valid, or there is no login info set for this user type");
        }

        loginPage.login(userProvider.getUser(userType));
    }

    @Then("^I will see the message: (.+)$")
    public void i_will_see_the_validation_message(String validationMessage) throws Throwable {
        switch (validationMessage) {
            case "Username or Password is incorrect":
                Assert.assertTrue(loginPage.getErrorMessage().contains(validationMessage));
                break;
            case "Login Successful":
                Assert.assertTrue(toolsPage.getLoginSuccessMessage().contains(validationMessage));
                break;
            default:
                Assert.fail("Either the message is incorrect or not shown");
        }
    }
}
