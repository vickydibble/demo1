package com.safebear.auto.pages.locators;

import lombok.Data;
import org.openqa.selenium.By;

@Data
public class ToolsPageLocators {

    private By successMessageLocator = By.xpath("/html/body/div/p[2]/b");
}
