package com.safebear.auto.utils;



import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.openqa.selenium.io.FileHandler.copy;

public class Properties {

    private static final String URL = System.getProperty("url", "http://toolslist.safebear.co.uk:8080/");
    private static final String BROWSER = System.getProperty("browser", "chrome");

    public static String getUrl() {
        return URL;
    }

    public static WebDriver getDriver() {
        System.setProperty("webdriver.chrome.driver", "src/test/resources/drivers/chromedriver.exe");
        System.setProperty("webdriver.gecko.driver", "src/test/resources/drivers/geckodriver.exe");

        ChromeOptions options = new ChromeOptions();
        options.addArguments("window-size=1366,768");

        switch (BROWSER) {
            case "chrome":
                return new ChromeDriver(options);
            case "firefox":
                return new FirefoxDriver();
            case "headless":
                options.addArguments("headless", "disable-gpu", "no-proxy-server", "proxy-server='direct://'", "proxy-bypass-list=*");
                return new ChromeDriver(options);
            default:
                return new ChromeDriver();
        }
    }

    public static String generateScreenshotFileName(String testName) {
        return testName + new SimpleDateFormat("yyy.MM.dd.HH.mm.ss").format(new Date())+ ".png";
    }

    public static void captureScreenShot(WebDriver driver, String fileName) {
        File screenFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        File file = new File("target/screenshots");
        if (!file.exists()) {
            if (file.mkdir()) {
                System.out.println("Directory created");
            } else {
                System.out.println("Directory failed");
            }
        }

        try {
            copy(screenFile, new File("target/screenshots/"+fileName));
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
