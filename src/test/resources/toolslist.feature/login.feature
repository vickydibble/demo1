Feature: Login
  In order to access my acc
  As a user
  I want to know if login is successful

  Rules:
  * User must be informed success
  * User must be informed if failed

  Glossary:
  * User

  @HighRisk
  @Regression
  Scenario Outline: Navigate and login to app
    Given I nav to the login page
    When I enter login details for <userType>
    Then I will see the message: <validationMessage>
    Examples:
      | userType    | validationMessage                  |
      | validUser   | Login Successful                   |
      | invalidUser | Username or Password is incorrect |


